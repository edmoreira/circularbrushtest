class Brush{
  float x;
  float y;
  float diametro;
  PImage img;
  float direccionx;
  float direcciony;
  int periodo;
  boolean esVisible;
  int contador;
  float angulo;
  int comportamiento;
  
  Brush(float tempX, float tempY, float tempD, float tmpdireccionx, float tmpdirecciony, int tmpPeriodo, PImage tempImg, int tempComp){
    x = tempX;
    y = tempY;
    diametro = tempD;
    img = tempImg;
    direccionx = tmpdireccionx;
    direcciony = tmpdirecciony;
    periodo = tmpPeriodo;
    esVisible = false;
    angulo = 0;
    comportamiento = tempComp;
  }
  
  void movAlt(int contador){
    if(contador%periodo == 0){
      direccionx = random(-2,2);
      direcciony = random(-2,2);
    }
    y = y + direcciony;
    x = x + direccionx;
  }
  
  void circular(float ancho, int centrox, int centroy, int dirx, int diry){
    //de alguna manera encontrar como fijar el centro del circulo para poder moverlo al presionar una tecla
    /*if(inicial == false){
      
      centrox = int(x);
      centroy = int(y);
      inicial = true;
    }*/
    x = centrox + cos(angulo)*ancho + direccionx;
    y = centroy + sin(angulo)*ancho + direcciony;
    
    if(dirx != 0){
      if(dirx > 0){
        direccionx = direccionx + 1;
      }else if(dirx < 0){
        direccionx = direccionx - 1;
      }
    }
    if(diry != 0){
      if(diry > 0){
        direcciony = direcciony +1;
      }else if(diry < 0){
        direcciony = direcciony -1;
      }
    }
    
    angulo = angulo + 0.01;
    if(angulo > TWO_PI){
      angulo = 0;
    }
  }
  
  void display(){
    imageMode(CENTER);
    img.resize(100,100);
    image(img,x,y);
  }
  
  void limites(){
    //rebota con el limite de arriba
    if(y<diametro/2){
      direcciony = -direcciony;
    }
    //rebota con el limite de abajo
    if(y>height-(diametro/2)){
      direcciony = -direcciony;
    }
    //rebota con el limite de la izquierda
    if(x<diametro/2){
      direccionx = -direccionx;
    }
    //rebota con el limite de la derecha
    if(x>width-(diametro/2)){
      direccionx = -direccionx;
    }
  }
  
  void reallocate(){
    //modifica la posicion actual del brush
    x = random(50,width);
    y = random(50,height);
  }
}
