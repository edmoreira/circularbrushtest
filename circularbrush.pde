PImage[] brushesImg = new PImage[2];
Brush[] brushes = new Brush[2];

void setup(){  
  fullScreen();
  background(255);
  //cargo las imagenes en el arreglo de imagenes
  for(int i = 0; i < brushes.length; i++){
    brushesImg[i] = loadImage(i+".png");
  }
  
  for(int i = 0; i < brushes.length; i++){
    //x, y, diametro, velX, velY, periodo, img, comportamiento del movimiento
    brushes[i] = new Brush(width/2, height/2, 10, 10, 10, 40, brushesImg[i], i);
  }
}

void draw(){
  imageMode(CENTER);
  for(int i = 0; i < brushes.length; i++){
    if(brushes[i].esVisible){
      if(brushes[i].comportamiento == 0){
        //movimiento aleatorio diagonal
        brushes[i].movAlt(int(random(10, 100)));
      }else if(brushes[i].comportamiento == 1){
        //ancho, centrox, centroy, mov en x, mov en y
        brushes[i].circular(200, 400, 400, 4, 0);
      }
      brushes[i].display();
      brushes[i].limites();
    }
  }
}

void keyPressed(){
  if(key == 'g'){
    save("savedata.png");
    print("Se guardo la imagen satisfactoriamente "+key);
  }
  if(key == 'q'){
    brushes[0].esVisible = true;
  }
  if(key == 'w'){
    brushes[1].esVisible = true;
  }
}

void keyReleased(){
  switch(keyCode){
    //al soltar las teclas se hace invisibles los brushes y se vuelven a ubicar aleatoriamente para la siguiente llamada
  case 'Q':
    brushes[0].esVisible = false;
    brushes[0].reallocate();
    return;
  case 'W':
    brushes[1].esVisible = false;
    brushes[1].reallocate();
    return;
  }
}
